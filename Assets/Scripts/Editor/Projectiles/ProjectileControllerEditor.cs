﻿using System;
using System.Linq;
using System.Reflection;
using PsiLib.Core.Collision.Abstract;
using PsiLib.Core.Projectiles;
using UnityEditor;
using UnityEngine;

namespace PsiLib.Editor.Projectiles
{
  [CustomEditor(typeof(ProjectileController), true)]
  public class ProjectileControllerEditor : CustomEditor<ProjectileController>
  {
    private Type[] collisionHandlerTypes;
    private int guiSelectedIdx;

    private string[] guiStrings;

    protected override void Awake()
    {
      base.Awake();
      Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
      collisionHandlerTypes = assemblies
        .SelectMany(assembly => assembly.ExportedTypes)
        .Where(type => typeof(ICollisionHandler).IsAssignableFrom(type))
        .Where(type => !type.IsAbstract && !type.IsInterface && !type.IsGenericTypeDefinition)
        .ToArray();
      guiStrings = collisionHandlerTypes.Select(type => type.Name).ToArray();
    }

    protected override void CustomInspectorGUI()
    {
      base.CustomInspectorGUI();
      guiSelectedIdx = EditorGUILayout.Popup("Add new ICollisionHandler", guiSelectedIdx, guiStrings);
      if (GUILayout.Button("Add"))
        Target.gameObject.AddComponent(collisionHandlerTypes[guiSelectedIdx]);
    }

    protected override void LayoutDebugInfo() { }
  }
}
