﻿using PsiLib.Core.Character.Health;
using UnityEditor;

namespace PsiLib.Editor.Character.Health
{
  [CustomEditor(typeof(HealthController))]
  public class HealthControllerEditor : CustomEditor<HealthController>
  {
    protected override void LayoutDebugInfo()
    {
      EditorGUILayout.LabelField("Health: ", $"{Target.CurrentHealth}/{Target.MaxHealth}");
    }
  }
}
