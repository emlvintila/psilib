﻿using PsiLib.Core.Character.Movement.Supplier.Abstract;
using UnityEditor;

namespace PsiLib.Editor.Character.Movement.Supplier
{
  [CustomEditor(typeof(AxisSupplierBase), true)]
  public class AxisSupplierEditor : CustomEditor<AxisSupplierBase>
  {
    protected override void LayoutDebugInfo()
    {
      EditorGUILayout.LabelField("", Target.GetVector().ToString());
    }
  }
}
