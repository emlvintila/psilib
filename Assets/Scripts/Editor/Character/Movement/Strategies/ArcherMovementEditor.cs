﻿using PsiLib.Core.Character.Movement.Strategies;
using PsiLib.Core.Character.Movement.Strategies.Abstract;
using UnityEditor;

namespace PsiLib.Editor.Character.Movement.Strategies
{
  [CustomEditor(typeof(ArcherMovement), true)]
  public class ArcherMovementEditor : CustomEditor<ArcherMovement>
  {
    protected override void LayoutDebugInfo()
    {
      IMovementStrategyVelocity movement = Target;
      IRotationStrategyTarget rotation = Target;

      EditorGUILayout.LabelField("Movement Velocity", movement.Velocity.ToString());
      EditorGUILayout.LabelField("Rotation Target", rotation.Target.ToString());
    }
  }
}
