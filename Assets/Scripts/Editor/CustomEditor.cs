﻿using UnityEditor;
using UnityEngine;

namespace PsiLib.Editor
{
  public abstract class CustomEditor<T> : UnityEditor.Editor
    where T : Object
  {
    private bool debugFoldout;

    protected T Target { get; private set; }

    protected virtual void Awake()
    {
      Target = (T) target;
    }

    public sealed override void OnInspectorGUI()
    {
      CustomInspectorGUI();
      debugFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(debugFoldout, "Debug");
      if (debugFoldout)
      {
        ++EditorGUI.indentLevel;
        LayoutDebugInfo();
        --EditorGUI.indentLevel;
      }

      EditorGUILayout.EndFoldoutHeaderGroup();
    }

    public override bool RequiresConstantRepaint() => debugFoldout;

    protected virtual void LayoutDebugInfo() { }

    protected virtual void CustomInspectorGUI()
    {
      base.OnInspectorGUI();
    }
  }
}
