﻿using System.Globalization;
using PsiLib.Core.Inventory;
using UnityEditor;

namespace PsiLib.Editor.Inventory
{
  [CustomEditor(typeof(InventoryWeightedList))]
  public class InventoryEditor : CustomEditor<InventoryWeightedList>
  {
    protected override void LayoutDebugInfo()
    {
      EditorGUILayout.LabelField(nameof(Target.GetCurrentWeight), Target.GetCurrentWeight().ToString(CultureInfo.InvariantCulture));
      EditorGUILayout.LabelField(nameof(Target.MaxWeight), Target.MaxWeight.Value.ToString(CultureInfo.InvariantCulture));
    }
  }
}
