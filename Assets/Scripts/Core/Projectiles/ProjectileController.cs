﻿using System.Collections.Generic;
using PsiLib.Core.Collision.Abstract;
using PsiLib.Core.Pooling;
using PsiLib.Core.Pooling.Abstract;
using UnityEngine;

namespace PsiLib.Core.Projectiles
{
  [RequireComponent(typeof(Rigidbody))]
  public class ProjectileController : MonoBehaviour, IPoolable
  {
    [SerializeField]
    private float lifetime;

    private readonly List<ICollisionHandler> collisionHandlers = new List<ICollisionHandler>();

    public Rigidbody Rigidbody { get; private set; }

    protected virtual void Awake()
    {
      Rigidbody = GetComponent<Rigidbody>();
      collisionHandlers.AddRange(GetComponents<ICollisionHandler>());
    }

    protected virtual void OnEnable()
    {
      if (Mathf.Approximately(lifetime, 0))
        return;

      Invoke(nameof(ReturnSelfToPool), lifetime);
    }

    protected virtual void OnDisable()
    {
      CancelInvoke(nameof(ReturnSelfToPool));
    }

    protected virtual void OnCollisionEnter(UnityEngine.Collision collision)
    {
      // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
      foreach (ICollisionHandler handler in collisionHandlers)
      {
        bool handled = handler.HandleCollision(gameObject, collision);
        if (handled)
          break;
      }
    }

    public IObjectPool Pool { get; set; }

    public virtual void ResetSelf()
    {
      Rigidbody.velocity = Vector3.zero;
      Rigidbody.angularVelocity = Vector3.zero;
    }

    private void ReturnSelfToPool()
    {
      this.ReturnToPool();
    }
  }
}
