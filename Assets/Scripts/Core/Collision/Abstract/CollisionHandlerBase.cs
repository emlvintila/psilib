﻿using UnityEngine;

namespace PsiLib.Core.Collision.Abstract
{
  public abstract class CollisionHandlerBase : MonoBehaviour, ICollisionHandler
  {
    [SerializeField]
    private bool stopHandling;

    public bool HandleCollision(GameObject gameObject, UnityEngine.Collision collision)
    {
      if (!CanHandle(gameObject, collision))
        return false;

      DoHandleCollision(gameObject, collision);

      return stopHandling;
    }

    protected abstract bool CanHandle(GameObject gameObject, UnityEngine.Collision collision);

    protected abstract void DoHandleCollision(GameObject gameObject, UnityEngine.Collision collision);
  }
}
