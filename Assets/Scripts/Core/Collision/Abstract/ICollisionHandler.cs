﻿using UnityEngine;

namespace PsiLib.Core.Collision.Abstract
{
  public interface ICollisionHandler
  {
    bool HandleCollision(GameObject gameObject, UnityEngine.Collision collision);
  }
}
