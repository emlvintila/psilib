﻿using System.Collections.Generic;
using PsiLib.Core.Collision.Abstract;
using PsiLib.Core.Pooling;
using PsiLib.Core.Projectiles;
using UnityEngine;

namespace PsiLib.Core.Collision
{
  public class ProjectileTagCollisionHandler : CollisionHandlerBase
  {
    [SerializeField]
    private string[] tags;

    private HashSet<string> tagSet;

    private void Awake() => tagSet = new HashSet<string>(tags);

    protected override bool CanHandle(GameObject gameObject, UnityEngine.Collision collision) => tagSet.Contains(collision.gameObject.tag);

    protected override void DoHandleCollision(GameObject gameObject, UnityEngine.Collision collision)
    {
      ProjectileController projectileController = gameObject.GetComponent<ProjectileController>();
      projectileController.ReturnToPool();
    }
  }
}
