﻿using PsiLib.Core.Pooling.Abstract;

namespace PsiLib.Core.Pooling
{
  public static class PoolingExtensions
  {
    public static void ReturnToPool(this IPoolable obj) => obj.Pool.ReturnToPool(obj);
  }
}
