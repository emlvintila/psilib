﻿namespace PsiLib.Core.Pooling.Abstract
{
  public interface IPoolable : IResettable
  {
    IObjectPool Pool { get; set; }
  }
}
