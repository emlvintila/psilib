﻿namespace PsiLib.Core.Pooling.Abstract
{
  public interface IObjectPool
  {
    void ReturnToPool(IPoolable obj);

    IPoolable GetObject();
  }
}
