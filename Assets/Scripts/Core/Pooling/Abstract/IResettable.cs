﻿namespace PsiLib.Core.Pooling.Abstract
{
  public interface IResettable
  {
    void ResetSelf();
  }
}
