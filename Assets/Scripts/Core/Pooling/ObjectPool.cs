﻿using System;
using System.Collections.Generic;
using PsiLib.Core.Pooling.Abstract;
using UnityEngine;

namespace PsiLib.Core.Pooling
{
  public class ObjectPool : MonoBehaviour, IObjectPool
  {
    [SerializeField]
    private int initialSize;

    [SerializeField]
    private GameObject prefab;

    private readonly Queue<IPoolable> objects = new Queue<IPoolable>();

    private void Awake()
    {
      if (prefab.GetComponent<IPoolable>() == null)
        throw new Exception("The prefab doesn't have an IPoolable component.");

      for (int i = 0; i < initialSize; ++i)
        ReturnToPool(CreateObject());
    }

    public void ReturnToPool(IPoolable obj)
    {
      objects.Enqueue(obj);
      DisableObject(obj);
    }

    public IPoolable GetObject()
    {
      IPoolable obj = objects.Count > 0 ? objects.Dequeue() : CreateObject();
      EnableObject(obj);

      return obj;
    }

    private IPoolable CreateObject()
    {
      GameObject go = Instantiate(prefab, transform);
      IPoolable obj = go.GetComponent<IPoolable>();
      obj.Pool = this;
      DisableObject(obj);

      return obj;
    }

    private static void DisableObject(IPoolable obj)
    {
      Component component = (Component) obj;
      component.gameObject.SetActive(false);
    }

    private static void EnableObject(IPoolable obj)
    {
      obj.ResetSelf();
      Component component = (Component) obj;
      component.gameObject.SetActive(true);
    }
  }
}
