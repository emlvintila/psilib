﻿using System.Collections.Generic;

namespace PsiLib.Core.Inventory
{
  public class ItemEqualityComparer : IEqualityComparer<Item>
  {
    public bool Equals(Item x, Item y) => new ItemObjectEqualityComparer().Equals(x?.ItemObject, y?.ItemObject);

    public int GetHashCode(Item obj) => obj.ItemObject.GetHashCode();
  }
}
