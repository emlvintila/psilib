﻿namespace PsiLib.Core.Inventory
{
  public class Item
  {
    public Item(ItemObjectBase itemObject) => ItemObject = itemObject;

    public ItemObjectBase ItemObject { get; }
  }
}
