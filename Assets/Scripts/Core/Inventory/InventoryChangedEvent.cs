﻿using System;
using UnityEngine.Events;

namespace PsiLib.Core.Inventory
{
  [Serializable]
  public class InventoryChangedEvent : UnityEvent<Item> { }
}
