﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace PsiLib.Core.Inventory
{
  public interface IInventory
  {
    UnityEvent<Item> OnItemAdded { get; }

    UnityEvent<Item> OnItemRemoved { get; }

    IEnumerable<Item> GetAllItems();

    bool ContainsItem(Item item);

    bool CanPutItem(Item item);

    Item PutItem(Item item);

    void RemoveItem(Item item);
  }
}
