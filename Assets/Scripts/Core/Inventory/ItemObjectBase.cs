﻿using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace PsiLib.Core.Inventory
{
  [CreateAssetMenu(menuName = "Items/New Item")]
  public abstract class ItemObjectBase : ScriptableObject
  {
    [SerializeField]
    private long id;

    [SerializeField]
    private string itemName;

    [SerializeField]
    private float weight;

    public long Id => id;

    public string ItemName => itemName;

    public float Weight => weight;

    [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode", Justification = "SerializedFields are not going to be modified at runtime")]
    public override int GetHashCode() => id.GetHashCode() ^ itemName.GetHashCode() ^ weight.GetHashCode();
  }
}
