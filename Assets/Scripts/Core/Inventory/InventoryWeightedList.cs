﻿using System;
using System.Collections.Generic;
using System.Linq;
using PsiLib.Core.Properties;
using UnityEngine;
using UnityEngine.Events;

namespace PsiLib.Core.Inventory
{
  public class InventoryWeightedList : MonoBehaviour, IInventory
  {
    [SerializeField]
    private MultipliablePropertyFloat maxWeight = new MultipliablePropertyFloat();

    [SerializeField]
    private InventoryChangedEvent onItemAdded = new InventoryChangedEvent();

    [SerializeField]
    private InventoryChangedEvent onItemRemoved = new InventoryChangedEvent();

    private readonly IList<Item> items = new List<Item>();

    public MultipliablePropertyFloat MaxWeight => maxWeight;

    public UnityEvent<Item> OnItemAdded => onItemAdded;

    public UnityEvent<Item> OnItemRemoved => onItemRemoved;

    public IEnumerable<Item> GetAllItems() => items;

    public bool ContainsItem(Item item)
    {
      Item existing = GetExistingItemOrNull(item);
      return existing != null;
    }

    public bool CanPutItem(Item item) => GetCurrentWeight() + item.ItemObject.Weight <= maxWeight;

    public Item PutItem(Item item)
    {
      if (!CanPutItem(item))
        throw new InvalidOperationException();

      items.Add(item);
      OnItemAdded.Invoke(item);
      return item;
    }

    public void RemoveItem(Item item)
    {
      if (!ContainsItem(item))
        throw new InvalidOperationException();

      items.Remove(item);
      OnItemRemoved?.Invoke(item);
    }

    public float GetCurrentWeight() => items.Select(item => item.ItemObject.Weight).Sum();

    private Item GetExistingItemOrNull(Item item) => items.FirstOrDefault(GetBoundItemEqualityPredicate(item));

    private static Func<Item, bool> GetBoundItemEqualityPredicate(Item item) => BoundComparer<Item>.Bind<ItemEqualityComparer>(item).EqualsOther;
  }
}
