﻿using System.Collections.Generic;
using System.Diagnostics;

namespace PsiLib.Core.Inventory
{
  public class ItemObjectEqualityComparer : IEqualityComparer<ItemObjectBase>
  {
    public bool Equals(ItemObjectBase x, ItemObjectBase y)
    {
      bool xNull = !x;
      bool yNull = !y;
      bool bothNotNull = !xNull && !yNull;
      if (!bothNotNull)
        return xNull ^ yNull;

      Debug.Assert(x != null, nameof(x) + " != null");
      Debug.Assert(y != null, nameof(y) + " != null");
      return x.Id.Equals(y.Id);
    }

    public int GetHashCode(ItemObjectBase obj) => obj.Id.GetHashCode();
  }
}
