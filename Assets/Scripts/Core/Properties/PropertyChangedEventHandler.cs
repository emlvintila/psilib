﻿using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties
{
  public delegate void PropertyChangedEventHandler<in TProperty, in TValue>(TProperty property, TValue oldValue, TValue newValue)
    where TProperty : IMutableProperty<TValue>;
}
