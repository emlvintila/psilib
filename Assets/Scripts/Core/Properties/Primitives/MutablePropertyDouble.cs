using System;
using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties.Primitives
{
  [Serializable]
  public class MutablePropertyDouble : MutablePropertyBase<double>
  {
    public MutablePropertyDouble() : this(default) { }

    public MutablePropertyDouble(double value) : base(value) { }
  }
}
