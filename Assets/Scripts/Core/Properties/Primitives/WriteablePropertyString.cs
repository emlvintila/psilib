using System;
using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties.Primitives
{
  [Serializable]
  public class WriteablePropertyString : WriteablePropertyBase<string>
  {
    public WriteablePropertyString() : this(default) { }

    public WriteablePropertyString(string value) : base(value) { }
  }
}
