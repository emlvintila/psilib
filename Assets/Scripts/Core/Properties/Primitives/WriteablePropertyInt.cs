using System;
using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties.Primitives
{
  [Serializable]
  public class WriteablePropertyInt : WriteablePropertyBase<int>
  {
    public WriteablePropertyInt() : this(default) { }

    public WriteablePropertyInt(int value) : base(value) { }
  }
}
