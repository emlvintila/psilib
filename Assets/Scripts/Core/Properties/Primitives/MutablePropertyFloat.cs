using System;
using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties.Primitives
{
  [Serializable]
  public class MutablePropertyFloat : MutablePropertyBase<float>
  {
    public MutablePropertyFloat() : this(default) { }

    public MutablePropertyFloat(float value) : base(value) { }
  }
}
