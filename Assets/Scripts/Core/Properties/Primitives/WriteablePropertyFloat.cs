using System;
using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties.Primitives
{
  [Serializable]
  public class WriteablePropertyFloat : WriteablePropertyBase<float>
  {
    public WriteablePropertyFloat() : this(default) { }

    public WriteablePropertyFloat(float value) : base(value) { }
  }
}
