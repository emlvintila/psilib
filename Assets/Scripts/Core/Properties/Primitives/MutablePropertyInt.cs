using System;
using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties.Primitives
{
  [Serializable]
  public class MutablePropertyInt : MutablePropertyBase<int>
  {
    public MutablePropertyInt() : this(default) { }

    public MutablePropertyInt(int value) : base(value) { }
  }
}
