using System;
using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties.Primitives
{
  [Serializable]
  public class MutablePropertyString : MutablePropertyBase<string>
  {
    public MutablePropertyString() : this(default) { }

    public MutablePropertyString(string value) : base(value) { }
  }
}
