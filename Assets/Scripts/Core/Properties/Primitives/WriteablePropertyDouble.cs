using System;
using PsiLib.Core.Properties.Abstract;

namespace PsiLib.Core.Properties.Primitives
{
  [Serializable]
  public class WriteablePropertyDouble : WriteablePropertyBase<double>
  {
    public WriteablePropertyDouble() : this(default) { }

    public WriteablePropertyDouble(double value) : base(value) { }
  }
}
