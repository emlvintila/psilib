﻿using System;
using PsiLib.Core.Properties.Abstract;
using PsiLib.Core.Properties.Primitives;
using UnityEngine;

namespace PsiLib.Core.Properties
{
  [Serializable]
  public class MultipliablePropertyFloat : MutablePropertyBase<float>
  {
    [SerializeField]
    private WriteablePropertyFloat additional;

    [SerializeField]
    private MutablePropertyFloat @base;

    [SerializeField]
    private WriteablePropertyFloat multiplier = new WriteablePropertyFloat(1);

    public IWriteableProperty<float> Additional => additional;

    public IMutableProperty<float> Base => @base;

    public IWriteableProperty<float> Multiplier => multiplier;

    public override float Value => Base.Value * Multiplier.Value + Additional.Value;
  }
}
