﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PsiLib.Core.Properties.Abstract
{
  [Serializable]
  public abstract class WriteablePropertyBase<TValue> : IWriteableProperty<TValue>
  {
    [SerializeField]
    private TValue value;

    protected WriteablePropertyBase() : this(default) { }

    protected WriteablePropertyBase(TValue value) => this.value = value;

    public virtual TValue Value
    {
      get => value;
      set
      {
        if (EqualityComparer<TValue>.Default.Equals(this.value, value))
          return;

        TValue old = this.value;
        this.value = value;
        Changed?.Invoke(this, old, value);
      }
    }

    public event PropertyChangedEventHandler<IWriteableProperty<TValue>, TValue> Changed;

    public static implicit operator TValue(WriteablePropertyBase<TValue> property) => property.Value;

    event PropertyChangedEventHandler<IMutableProperty<TValue>, TValue> IPropertyChanged<TValue>.Changed
    {
      add => Changed += value;
      remove => Changed -= value;
    }
  }
}
