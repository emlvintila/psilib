﻿namespace PsiLib.Core.Properties.Abstract
{
  public interface IWriteableProperty<TValue> : IMutableProperty<TValue>, IPropertyChanged<TValue, IWriteableProperty<TValue>>
  {
    new TValue Value { get; set; }
  }
}
