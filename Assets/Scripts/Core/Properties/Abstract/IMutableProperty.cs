﻿namespace PsiLib.Core.Properties.Abstract
{
  public interface IMutableProperty<out TValue> : IProperty<TValue>, IPropertyChanged<TValue> { }
}
