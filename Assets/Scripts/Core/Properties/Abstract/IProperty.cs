﻿namespace PsiLib.Core.Properties.Abstract
{
  public interface IProperty<out TValue>
  {
    TValue Value { get; }
  }
}
