﻿namespace PsiLib.Core.Properties.Abstract
{
  public class PropertyBase<TValue> : IProperty<TValue>
  {
    public TValue Value { get; }

    public PropertyBase(TValue value) => Value = value;
  }
}
