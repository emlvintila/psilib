﻿using System;
using UnityEngine;

namespace PsiLib.Core.Properties.Abstract
{
  [Serializable]
  public abstract class MutablePropertyBase<TValue> : IMutableProperty<TValue>
  {
    [SerializeField]
    private TValue value;

    protected MutablePropertyBase() : this(default) { }

    protected MutablePropertyBase(TValue value) => this.value = value;

    public virtual TValue Value => value;

    public event PropertyChangedEventHandler<IMutableProperty<TValue>, TValue> Changed;

    public static implicit operator TValue(MutablePropertyBase<TValue> property) => property.Value;
  }
}
