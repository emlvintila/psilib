﻿namespace PsiLib.Core.Properties.Abstract
{
  public interface IPropertyChanged<out TValue, out TProperty> : IPropertyChanged<TValue>
    where TProperty : IMutableProperty<TValue>
  {
    new event PropertyChangedEventHandler<TProperty, TValue> Changed;
  }

  public interface IPropertyChanged<out TValue>
  {
    event PropertyChangedEventHandler<IMutableProperty<TValue>, TValue> Changed;
  }
}
