﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace PsiLib.Core
{
  public class BoundComparer<TObject>
  {
    public BoundComparer(TObject boundObject, [NotNull] IEqualityComparer<TObject> comparer)
    {
      Comparer = comparer;
      BoundObject = boundObject;
    }

    public IEqualityComparer<TObject> Comparer { get; }
    public TObject BoundObject { get; }

    public bool EqualsOther(TObject other) => Comparer.Equals(BoundObject, other);

    public static BoundComparer<TObject> Bind<TComparer>(TObject boundObject)
      where TComparer : IEqualityComparer<TObject>, new() =>
      new BoundComparer<TObject>(boundObject, new TComparer());
  }
}
