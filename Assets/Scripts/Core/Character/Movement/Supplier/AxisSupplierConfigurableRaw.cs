﻿using PsiLib.Core.Character.Movement.Supplier.Abstract;
using UnityEngine;

namespace PsiLib.Core.Character.Movement.Supplier
{
  public class AxisSupplierConfigurableRaw : AxisSupplierConfigurableBase
  {
    public override float GetX() => Input.GetAxisRaw(XAxis);

    public override float GetY() => Input.GetAxisRaw(YAxis);

    public override float GetZ() => Input.GetAxisRaw(ZAxis);
  }
}
