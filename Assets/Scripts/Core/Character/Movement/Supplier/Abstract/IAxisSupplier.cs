﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Supplier.Abstract
{
  public interface IAxisSupplier
  {
    float GetX();

    float GetY();

    float GetZ();

    Vector3 GetVector();
  }
}
