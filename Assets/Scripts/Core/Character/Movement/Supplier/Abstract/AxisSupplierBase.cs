﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Supplier.Abstract
{
  public abstract class AxisSupplierBase : MonoBehaviour, IAxisSupplier
  {
    public abstract float GetX();

    public abstract float GetY();

    public abstract float GetZ();

    public Vector3 GetVector() => new Vector3(GetX(), GetY(), GetZ());
  }
}
