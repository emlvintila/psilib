﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Supplier.Abstract
{
  public abstract class AxisSupplierConfigurableBase : AxisSupplierBase
  {
    [SerializeField]
    private string xAxis;

    [SerializeField]
    private string yAxis;

    [SerializeField]
    private string zAxis;

    protected string XAxis => xAxis;

    protected string YAxis => yAxis;

    protected string ZAxis => zAxis;
  }
}
