﻿using PsiLib.Core.Character.Movement.Supplier.Abstract;
using UnityEngine;

namespace PsiLib.Core.Character.Movement.Supplier
{
  public class AxisSupplierConfigurable : AxisSupplierConfigurableBase
  {
    public override float GetX() => Input.GetAxis(XAxis);

    public override float GetY() => Input.GetAxis(YAxis);

    public override float GetZ() => Input.GetAxis(ZAxis);
  }
}
