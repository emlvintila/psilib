﻿using System;
using UnityEngine;

namespace PsiLib.Core.Character.Movement
{
  public class PositionController : MonoBehaviour
  {
    private Func<Vector3> getPositionDelegate;
    private Func<Vector3> getRotationDelegate;
    private Action<Vector3> movePositionDelegate;
    private Action<Vector3> moveRotationDelegate;
    private new Rigidbody rigidbody;
    private new Rigidbody2D rigidbody2D;
    private Action<Vector3> setPositionDelegate;
    private Action<Vector3> setRotationDelegate;

    private void Awake()
    {
      rigidbody2D = GetComponent<Rigidbody2D>();
      rigidbody = GetComponent<Rigidbody>();
      if (rigidbody2D)
      {
        getPositionDelegate = () => rigidbody2D.position;
        getRotationDelegate = () => Vector3.forward * rigidbody2D.rotation;
        movePositionDelegate = position => rigidbody2D.MovePosition(position);
        moveRotationDelegate = rotation => rigidbody2D.MoveRotation(Quaternion.Euler(rotation));
        setPositionDelegate = position => rigidbody2D.position = position;
        setRotationDelegate = rotation => rigidbody2D.SetRotation(Quaternion.Euler(rotation));
      }
      else if (rigidbody)
      {
        getPositionDelegate = () => rigidbody.position;
        getRotationDelegate = () => rigidbody.rotation.eulerAngles;
        movePositionDelegate = position => rigidbody.MovePosition(position);
        moveRotationDelegate = rotation => rigidbody.MoveRotation(Quaternion.Euler(rotation));
        setPositionDelegate = position => rigidbody.position = position;
        setRotationDelegate = rotation => rigidbody.rotation = Quaternion.Euler(rotation);
      }
      else
      {
        getPositionDelegate = () => transform.position;
        getRotationDelegate = () => transform.rotation.eulerAngles;
        movePositionDelegate = position => transform.position = position;
        moveRotationDelegate = rotation => transform.rotation = Quaternion.Euler(rotation);
        setPositionDelegate = position => transform.position = position;
        setRotationDelegate = rotation => transform.rotation = Quaternion.Euler(rotation);
      }
    }

    public Vector3 GetPosition() => getPositionDelegate.Invoke();

    public Vector3 GetRotation() => getRotationDelegate.Invoke();

    public void MovePosition(Vector3 position) => movePositionDelegate.Invoke(position);

    public void MoveRotation(Vector3 rotation) => moveRotationDelegate.Invoke(rotation);

    public void SetPosition(Vector3 position) => setPositionDelegate.Invoke(position);

    public void SetRotation(Vector3 rotation) => setRotationDelegate.Invoke(rotation);
  }
}
