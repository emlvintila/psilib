﻿using PsiLib.Core.Character.Movement.Abstract;
using PsiLib.Core.Character.Movement.Strategies.Abstract;
using PsiLib.Core.Character.Movement.Validation.Abstract;
using UnityEngine;

namespace PsiLib.Core.Character.Movement.Strategies
{
  [RequireComponent(typeof(IPositionValidator))]
  public class ArcherMovement : MonoBehaviour, IMovementStrategyVelocity, IRotationStrategyTarget
  {
    [SerializeField]
    private Velocity movementVelocity;

    private IPositionValidator positionValidator;

    private void Awake()
    {
      positionValidator = GetComponent<IPositionValidator>();
    }

    Vector3 IMovementStrategy.GetPositionForNextFrame(Vector3 currentPosition)
    {
      Vector3 scaledVelocity = Vector3.Scale(movementVelocity.Current, movementVelocity.Multiplier);
      Vector3 targetPosition = scaledVelocity * Time.deltaTime + currentPosition;
      return positionValidator.IsValidOrGetClosestValidPosition(gameObject, targetPosition, out Vector3 validPosition) ? targetPosition : validPosition;
    }

    IVelocity IMovementStrategyVelocity.Velocity => movementVelocity;

    Vector3 IRotationStrategy.GetRotationForNextFrame(Vector3 currentRotation) => ((IRotationStrategyTarget) this).Target;

    Vector3 IRotationStrategyTarget.Target { get; set; }
  }
}
