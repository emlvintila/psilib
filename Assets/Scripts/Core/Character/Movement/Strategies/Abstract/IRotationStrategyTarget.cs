﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Strategies.Abstract
{
  public interface IRotationStrategyTarget : IRotationStrategy
  {
    Vector3 Target { get; set; }
  }
}
