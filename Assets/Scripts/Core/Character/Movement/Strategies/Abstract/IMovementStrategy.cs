﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Strategies.Abstract
{
  public interface IMovementStrategy
  {
    Vector3 GetPositionForNextFrame(Vector3 currentPosition);
  }
}
