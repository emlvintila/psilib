﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Strategies.Abstract
{
  public interface IRotationStrategy
  {
    Vector3 GetRotationForNextFrame(Vector3 currentRotation);
  }
}
