﻿using PsiLib.Core.Character.Movement.Abstract;

namespace PsiLib.Core.Character.Movement.Strategies.Abstract
{
  public interface IMovementStrategyVelocity : IMovementStrategy
  {
    IVelocity Velocity { get; }
  }
}
