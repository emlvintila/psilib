﻿using System;
using PsiLib.Core.Character.Movement.Abstract;
using UnityEngine;

namespace PsiLib.Core.Character.Movement
{
  [Serializable]
  public class Velocity : IVelocity
  {
    [SerializeField]
    private Vector3 multiplier = Vector3.one;

    private Vector3 current;

    public Vector3 Multiplier
    {
      get => multiplier;
      set => multiplier = value;
    }

    public Vector3 Current
    {
      get => current;
      set => current = value;
    }

    public override string ToString() => $"{Vector3.Scale(current, multiplier)}";
  }
}
