﻿using PsiLib.Core.Character.Movement.Validation.Abstract;
using UnityEngine;

namespace PsiLib.Core.Character.Movement.Validation
{
  public class PositionValidatorIdentity : MonoBehaviour, IPositionValidator
  {
    public bool IsValidOrGetClosestValidPosition(GameObject gameObject, Vector3 targetPosition, out Vector3 validPosition)
    {
      validPosition = targetPosition;
      return true;
    }
  }
}
