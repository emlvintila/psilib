﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Validation.Abstract
{
  public interface IPositionValidator
  {
    bool IsValidOrGetClosestValidPosition(GameObject gameObject, Vector3 targetPosition, out Vector3 validPosition);
  }
}
