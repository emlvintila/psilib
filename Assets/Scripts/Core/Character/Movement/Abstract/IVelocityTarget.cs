﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Abstract
{
  public interface IVelocityTarget : IVelocity
  {
    new Vector3 Current { get; }
    Vector3 Target { get; set; }
  }
}
