﻿using UnityEngine;

namespace PsiLib.Core.Character.Movement.Abstract
{
  public interface IVelocity
  {
    Vector3 Current { get; set; }
  }
}
