﻿using PsiLib.Core.Character.Movement;
using PsiLib.Core.Character.Movement.Strategies.Abstract;
using PsiLib.Core.Character.Movement.Supplier.Abstract;
using UnityEngine;

namespace PsiLib.Core.Character
{
  [RequireComponent(typeof(IAxisSupplier), typeof(IMovementStrategy), typeof(IRotationStrategy))]
  [RequireComponent(typeof(PositionController))]
  public abstract class CharacterControllerBase : MonoBehaviour
  {
    protected IMovementStrategy MovementStrategy;
    protected IAxisSupplier MovementSupplier;
    protected PositionController PositionController;
    protected IRotationStrategy RotationStrategy;
    protected IAxisSupplier RotationSupplier;
    protected Transform Transform;

    protected virtual void Awake()
    {
      IAxisSupplier[] axisSuppliers = GetComponents<IAxisSupplier>();
      Debug.Assert(axisSuppliers.Length == 2, "axisSuppliers.Length != 2", this);
      MovementSupplier = axisSuppliers[0];
      RotationSupplier = axisSuppliers[1];
      MovementStrategy = GetComponent<IMovementStrategy>();
      RotationStrategy = GetComponent<IRotationStrategy>();
      PositionController = GetComponent<PositionController>();
      Transform = transform;
    }

    protected virtual void Update()
    {
      UpdatePosition();
      UpdateRotation();
    }

    private void UpdatePosition()
    {
      UpdateMovementStrategy();
      Vector3 currentPosition = PositionController.GetPosition();
      Vector3 position = MovementStrategy.GetPositionForNextFrame(currentPosition);
      PositionController.MovePosition(position);
    }

    protected abstract void UpdateMovementStrategy();

    private void UpdateRotation()
    {
      UpdateRotationStrategy();
      Vector3 currentRotation = PositionController.GetRotation();
      Vector3 rotation = RotationStrategy.GetRotationForNextFrame(currentRotation);
      PositionController.MoveRotation(rotation);
    }

    protected abstract void UpdateRotationStrategy();
  }
}
