﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

namespace PsiLib.Core.Character.Health
{
  public class HealthController : MonoBehaviour
  {
    [SerializeField]
    private float maxHealth;

    [SerializeField]
    private HealthChangedEvent onHealthChanged;

    private float currentHealth;

    /// <summary>
    ///   Called with delta value (new health - old health).
    /// </summary>
    public HealthChangedEvent OnHealthChanged => onHealthChanged;

    public float MaxHealth => maxHealth;

    public float CurrentHealth
    {
      get => currentHealth;
      private set
      {
        float oldHealth = currentHealth;
        currentHealth = Mathf.Clamp(value, 0, maxHealth);
        float delta = currentHealth - oldHealth;
        onHealthChanged?.Invoke(delta);
      }
    }

    private void Awake()
    {
      CurrentHealth = maxHealth;
    }

    public void TakeDamage(float damage) => TakeDamage(damage, null);

    public void TakeDamage(float damage, [CanBeNull] Component component) => CurrentHealth -= damage;

    public void HealDamage(float healing) => HealDamage(healing, null);

    public void HealDamage(float healing, [CanBeNull] Component component) => CurrentHealth += healing;

    [Serializable]
    public class HealthChangedEvent : UnityEvent<float> { }
  }
}
